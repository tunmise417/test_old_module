using namespace System.Net

# Input bindings are passed in via param block.
param($Request, $TriggerMetadata)

# Write to the Azure Functions log stream.
Write-Host "PowerShell HTTP trigger function processing a deletion request."

# Interact with query parameters or the body of the request.
try {
    if ($null -ne $Request) {
        $subscription_id = $Request.Body.SubscriptionID
        $resource_group_name = $Request.Body.ResourceGroupName
        $gallery_name = $Request.Body.GalleryName
        $image_definition_name = $Request.Body.ImageDefinitionName
        $image_version = $Request.Body.ImageVersion

    }
    
    if (![string]::IsNullOrEmpty($subscription_id) -and ![string]::IsNullOrEmpty($resource_group_name) -and ![string]::IsNullOrEmpty($gallery_name) -and ![string]::IsNullOrEmpty($image_definition_name) -and ![string]::IsNullOrEmpty($image_version)) {
        Select-AzSubscription -Subscription $subscription_id
            $image_version = Get-AzGalleryImageVersion `
                -ResourceGroupName $resource_group_name `
                -GalleryName $gallery_name `
                -GalleryImageDefinitionName $image_definition_name `
                -GalleryImageVersionName $image_version `
                -ErrorAction SilentlyContinue
        if ($null -ne $image_version) {
            Write-Host "Deleting Image: $($image_version.Name) of Image Definition: $($image_definition_name) from Gallery: $($gallery_name)"
            
            Remove-AzGalleryImageVersion `
                -ResourceGroupName $resource_group_name `
                -GalleryName $gallery_name `
                -GalleryImageDefinitionName $image_definition_name `
                -Name $image_version `
                -Force
            Start-Sleep -s 10
            Write-Host "Deleted Image: $($image_version.Name) of Image Definition: $($image_definition_name) from Gallery: $($gallery_name)"
        
            $response = "Deleted Image: $($image_version.Name) of Image Definition: $($image_definition_name) from Gallery: $($gallery_name)"
        }
    
    
        # Associate values to output bindings by calling 'Push-OutputBinding'.
        Push-OutputBinding -Name Response -Value ([HttpResponseContext]@{
                StatusCode = [HttpStatusCode]::OK
                Body       = $response
            })
    }
    else {
        Push-OutputBinding -Name Response -Value ([HttpResponseContext]@{
                StatusCode = [HttpStatusCode]::BadRequest
                Body       = "Invalid Inputs: Any of expected input is either empty or null"
            })
    }
    Write-Host "PowerShell HTTP trigger function processed a deletion request."
}
catch {
    { Write-Error $_.Exception.Messege }
}
