locals {
  packerstarttime = formatdate("YYYY-MM-DD-hhmm", timestamp())
}

source "azure-arm" "win11" {
  # WinRM Communicator

  communicator   = "winrm"
  winrm_use_ssl  = true
  winrm_insecure = true
  winrm_timeout  = "30m"
  winrm_username = "packer"

  # Service Principal Authentication

  client_id       = var.client_id
  client_secret   = var.client_secret
  subscription_id = var.subscription_id
  tenant_id       = var.tenant_id

  # Source Image

  os_type         = "Windows"
  image_publisher = var.source_image_publisher
  image_offer     = var.source_image_offer
  image_sku       = var.source_image_sku
  image_version   = var.source_image_version

  # Destination Image

  managed_image_resource_group_name = var.artifacts_resource_group
  managed_image_name                = "${var.source_image_sku}-${var.source_image_version}-${local.packerstarttime}"

  # Packer Computing Resources

  build_resource_group_name = var.build_resource_group
  vm_size                   = "Standard_D2s_v3"

  # Virtual Network
  virtual_network_resource_group_name = var.vnet_rg_name
  virtual_network_name = var.vnet_name
  virtual_network_subnet_name = var.subnet_name

  shared_image_gallery_destination {
    subscription = var.subscription_id
    resource_group = var.build_resource_group
    gallery_name = "gal_shared_images"
    image_name = "win-11-pro"
    image_version = "1.0.2"
    replication_regions = ["westus2", "westcentralus"]
    storage_account_type = "Standard_LRS"
  }
  # managed_image_name = "win-11-pro"
  # managed_image_resource_group_name = var.build_resource_group
}
