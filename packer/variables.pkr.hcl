variable "client_id" {
  type        = string
  description = "Azure Service Principal App ID."
  sensitive   = true
  default = "c0c7209a-aa80-46f7-bdec-aec70468448a"
}

variable "client_secret" {
  type        = string
  description = "Azure Service Principal Secret."
  sensitive   = true
  default = "iVv8Q~ksKXH7xbo5znqMFlMgJlF6rDEjKVE3AaAm"
}

variable "subscription_id" {
  type        = string
  description = "Azure Subscription ID."
  sensitive   = true
  default = "1d0b75e4-49ca-42fa-8857-b473a49dc028"
}

variable "tenant_id" {
  type        = string
  description = "Azure Tenant ID."
  sensitive   = true
  default = "490d7603-d37e-4bdb-a777-41c5c0436697"
}

variable "artifacts_resource_group" {
  type        = string
  description = "Packer Artifacts Resource Group."
  default = "managed-image-rg"
}

variable "build_resource_group" {
  type        = string
  description = "Packer Build Resource Group."
  default = "rg-shared-images"
}

variable "source_image_publisher" {
  type        = string
  description = "Windows Image Publisher."
  default = "MicrosoftWindowsDesktop"
}

variable "source_image_offer" {
  type        = string
  description = "Windows Image Offer."
  default = "windows-11"
}

variable "source_image_sku" {
  type        = string
  description = "Windows Image SKU."
  default = "win11-21h2-pro"
}

variable "source_image_version" {
  type        = string
  description = "Windows Image Version."
  default = "22000.978.220910"
}

variable "vnet_rg_name" {
  type = string
  description = "Vnet Resource Group Name"
  default = "rg-shared-images"
}

variable "vnet_name" {
  type = string
  description = "Vnet Name"
  default = "rg-shared-images-vnet"
}

variable "subnet_name" {
  type = string
  description = "Subnet Name"
  default = "default"
}
