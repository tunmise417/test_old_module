cleanup:
  stage: cleanup
  image:
    name: mcr.microsoft.com/azure-powershell:latest
  script:
    - cd packer/win11-base/scripts && ./remove_az_resource.ps1 -SubscriptionID $(SubscriptionID) -ResourceGroupName $(Resourcegroupname) -ResourceName $(ResourceName)
  variables:
    SubscriptionID: 666d6f75-71fd-40c9-87d9-490b60a1fd
    ResourceGroupName: rg-managed-images-prod
    ResourceName: win*
  tags:
  - linux-docker