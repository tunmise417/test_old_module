﻿<#
    .DESCRIPTION
        This script removes the given resource from Azure 
    .NOTES
        AUTHOR: 
        LAST EDIT: 13 10, 2022
    .EXAMPLE
        .\remove_azure_resource.ps1 `
            -SubscriptionID "your-subscription-id" `
            -ResourceGroupName "your-resource-group" `
            -GalleryName "your-gallery-name" `
            -ImageDefinitionName "your-image-def-name" `
            -ImageVersion "your-image-version" `
            -ClientId "SPN-Client-Id" ` # this is optional. Use this if using spn for authentication
            -ClientSecret "SPN-Client-Secret" ` # this is optional. Use this if using spn for authentication
            -TenantId "your-tenant-id" # this is optional. Use this if using spn for authentication
#>

param 
(
    [Parameter(Mandatory=$true)]
    [string]$SubscriptionID,
    [Parameter(Mandatory=$true)]
    [string]$ResourceGroupName,
    [Parameter(Mandatory=$true)]
    [string]$GalleryName,
    [Parameter(Mandatory=$true)]
    [string]$ImageDefinitionName,
    [Parameter(Mandatory=$true)]
    [string]$ImageVersion,
    [Parameter(Mandatory=$false)]
    [string]$ClientId,
    [Parameter(Mandatory=$false)]
    [string]$ClientSecret,
    [Parameter(Mandatory=$false)]
    [string]$TenantId

 )

<#-- Initialize Connection & Import Modules --#>
Import-Module -Name Az.Resources
Import-Module -Name Az.Accounts

try
{
    if($null -ne $clientId)
    {
     $SecurePassword=ConvertTo-SecureString $ClientSecret -AsPlainText -Force
     $Credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $ClientId, $SecurePassword
     Connect-AzAccount -ServicePrincipal -TenantId $TenantId -Credential $Credential
    }
    else
    {
     Connect-AzAccunt -TenantId $TenantId
    }


      if (![string]::IsNullOrEmpty($SubscriptionID) -and ![string]::IsNullOrEmpty($ResourceGroupName) -and ![string]::IsNullOrEmpty($GalleryName) -and ![string]::IsNullOrEmpty($ImageDefinitionName) -and ![string]::IsNullOrEmpty($ImageVersion)) {
        Select-AzSubscription -Subscription $subscription_id
            $image_version = Get-AzGalleryImageVersion `
                -ResourceGroupName $ResourceGroupName `
                -GalleryName $GalleryName `
                -GalleryImageDefinitionName $ImageDefinitionName `
                -GalleryImageVersionName $ImageVersion `
                -ErrorAction SilentlyContinue
        if ($null -ne $image_version) {
            Write-Host "Deleting Image: $($image_version.Name) of Image Definition: $($ImageDefinitionName) from Gallery: $($GalleryName)"
            
            Remove-AzGalleryImageVersion `
                -ResourceGroupName $ResourceGroupName `
                -GalleryName $GalleryName `
                -GalleryImageDefinitionName $ImageDefinitionName `
                -Name $image_version.Name `
                -Force
   
            Write-Host "Deleted Image: $($image_version.Name) of Image Definition: $($ImageDefinitionName) from Gallery: $($GalleryName)"
        }
    }
    else {
        Write-Host "Invalid Inputs: Any of expected input is either empty or null"
    }
}
catch
{
 Write-Error $_.Exception.Messege
}