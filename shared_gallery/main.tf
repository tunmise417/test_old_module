resource "azurerm_shared_image_gallery" "gal" {
  name                = "gal_shared_images"
  resource_group_name = "rg-shared-images"
  location            = "westus2"
  description         = "Shared images"

  tags = var.common_tags
}

resource "azurerm_shared_image" "win11-img" {
  name                = "win-11-pro"
  gallery_name        = azurerm_shared_image_gallery.gal.name
  resource_group_name = "rg-shared-images"
  location            = "westus2"
  os_type             = "Windows"
  hyper_v_generation  = "V2"

  identifier {
    publisher = "alteryx"
    offer     = "windows-11"
    sku       = "ayx-win11-pro"
  }
  tags = var.common_tags
}
