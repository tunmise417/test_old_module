terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.0"
    }
  }
  required_version = ">= 1.1"
}

provider "azurerm" {
  features {}
}

variable "common_tags" {
  default = {
    costalloc   = "200"
    department  = "infra"
    environment = "prod"
    owner       = "ayoola417@gmail.com"
    project     = "my-stuff"
    terraform   = "true"
  }
  type = map(string)
}